
wishlist = [
    {:name => "mini puzzle", :size => "small", :clatters => "yes", :weight => "light"},
    {:name => "toy car", :size => "medium", :clatters => "a bit", :weight => "medium"},
    {:name => "card game", :size => "small", :clatters => "no", :weight => "light"}
]

presents = [
    {:size => "medium", :clatters => "a bit", :weight => "medium"},
    {:size => "small", :clatters => "yes", :weight => "light"}
]

def guess_gifts(wishlist, presents)
  guessed_gifts = []
  guessed_gift_list = wishlist.inject([]) { |gift, hash| gift << hash if presents.any? { |hash2| hash[:size] == hash2[:size] && hash[:clatters] == hash2[:clatters] && hash[:weight] == hash2[:weight] }; gift }

  guessed_gift_list.each do |gift|
    guessed_gifts.push(gift[:name])
  end
  puts guessed_gifts
end

guess_gifts(wishlist, presents) # must return ['toy car', 'mini puzzle']